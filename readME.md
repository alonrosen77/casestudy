In this file I'll explain the solutions for the exercises.
I did all 4 exercises successfully and tried solving the first
bonus but didn't successed.
first exercise:
I created a k8s cluster on digitalocean and configured 
kubectl using kubeconfig file. the cluster components:
external service - handle all the traffic.
mongo-express pod - gets the requests from the external service.
configmap - holds the database url
secret - hold credentials
(the configmap and secret used for commmunication between the pods)
mongodb pod - gets requests from mongo-express pod.
clusterIP service - get all request from with in the cluster and talk to
the mongodb pod.


second exercise: 
I created main.go file.the program will use the kubeconfig file
to connect to the cluster and then it will create a list of all the pods
in the default namespace. after that, the program will print each name of each
pod.

third exercise:
I created a Dockerfile for the go application,then built it to a docker image
and then pushed to dockerhub. after that, I created a helm chart that has
a pod (from the images I created) and external service.

fourth exercise:
I created a bash file (namespace.sh) which ask the user which name he
wants for the namespace. after the script has the name as variable it will
insert the name to the values.yaml file. after that, I configured all the
cluster components to take the namespace from the valus.yaml file and the 
script wiil update the helm chart accordingly.

fifth exrcise:
I didn't successed the bouns but wanted to explain my logic. I created
an application.yaml file to set up argo CD. I put that file inside the
templates folder of the chart, so when I update my chart it will apply
the application.yaml file. then I created another bash file that will
ask the user to what name to change the namespace and after the script
has the name it will enter an if statement only once to update the chart
accordingly and to activate argo CD. 

