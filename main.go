package main

 import (
   "context"
   "fmt"

   metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
   "k8s.io/client-go/kubernetes"
   "k8s.io/client-go/tools/clientcmd"
   )

   func main() {
	   rules := clientcmd.NewDefaultClientConfigLoadingRules()
	   kubeconfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rules, &clientcmd.ConfigOverrides{})
	   config, err := kubeconfig.ClientConfig()
	   if err != nil {
		 panic(err)
	 }
	   clientset := kubernetes.NewForConfigOrDie(config)

	   podList, err := clientset.CoreV1().Pods("default").List(context.Background(), metav1.ListOptions{})
	   if err != nil {
		 panic(err)
	 }
	 for _, p := range podList.Items {
		 fmt.Println(p.Name)
	 }
   }


