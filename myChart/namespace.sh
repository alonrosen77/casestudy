#!/bin/bash

#enter value for namespace
read -p "enter name for namespace:" namespace

echo namespace: $namespace > values.yaml
kubectl create namespace $namespace

#update the namespace in all components
helm upgrade -f values.yaml example .
