# syntax=docker/dockerfile:1

FROM golang:1.16-alpine

WORKDIR /root/home

RUN mkdir /root/.kube && cd /root/.kube && touch config && cd ..

COPY .kube/config /root/.kube/config

WORKDIR /home

RUN apk --no-cache add curl

RUN apk add --no-cache bash

RUN apk add sudo

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl

RUN chmod +x ./kubectl

RUN  mv ./kubectl /usr/local/bin/kubectl

ENV KUBE_CONFIG_PATH=/root/.kube/config

COPY . .

RUN go mod download

CMD ["go","run","main.go"]

